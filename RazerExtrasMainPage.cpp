#include "RazerExtrasMainPage.h"
#include <hidapi.h>
#include "RazerDevices.h"
#include "RazerChromaAddressableExtras.h"
#include "ui_RazerExtrasMainPage.h"

RazerExtrasMainPage::RazerExtrasMainPage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RazerExtrasMainPage)
{
    ui->setupUi(this);
    ui->tabWidget->clear();

    hid_device_info*    hid_devices         = NULL;
    hid_devices = hid_enumerate(RAZER_VID, 0);

    hid_device_info*    current_hid_device;
    current_hid_device  = hid_devices;

    bool found = false;

    while(current_hid_device)
    {
        if(current_hid_device->vendor_id == RAZER_VID && current_hid_device->product_id == RAZER_CHROMA_ADDRESSABLE_RGB_CONTROLLER_PID && current_hid_device->interface_number == 0x00)
        {
            hid_device* dev = hid_open_path(current_hid_device->path);
            RazerChromaAddressableExtras* razer_chroma_extras = new RazerChromaAddressableExtras(this, dev);
            ui->tabWidget->insertTab(ui->tabWidget->count(), razer_chroma_extras , "Razer Chroma");
            found = true;
        }

        current_hid_device = current_hid_device->next;
    }

    ui->no_devices->setVisible(!found);
    ui->tabWidget->setVisible(found);
}

RazerExtrasMainPage::~RazerExtrasMainPage()
{
    delete ui;
}
